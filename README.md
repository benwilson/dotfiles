# DOTFILES #
Dotfiles for bash and vim.

## To Install ##
Clone the repository and run install.sh to create symlinks. If your dotfiles
are already symlinks, the links will be deleted. If they exist already,
they'll have .bak appended.

