#!/usr/bin/env bash

# Get some settings
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd )"


function link_dotfiles()
{
    for FILE in $(ls "${DIR}/dotfiles")
    do
        if [[ -h "$HOME/.$FILE" ]]
        then
            rm "$HOME/.$FILE"
        fi

        if [[ -e "$HOME/.$FILE" ]]
        then
            echo "Backing up $HOME/.$FILE"
            mv "$HOME/.$FILE" "$HOME/.$FILE.bak"
        fi

        echo "Linking $FILE into $HOME"
        ln -s "${DIR}/dotfiles/${FILE}" "${HOME}/.${FILE}"
    done
}


function update_vim_plugins()
{
    if [[ ! -e "${DIR}/dotfiles/vim/bundle/Vundle.vim" ]]
    then
        git clone https://github.com/VundleVim/Vundle.vim.git "${DIR}/dotfiles/vim/bundle/Vundle.vim"
    fi

    vim +PluginUpdate +qall
}


function usage()
{
    echo "Install dotfiles and various supporting files"
    echo ""
    echo "OPTIONS:"
    echo -e "\t-h\tShow this message"
    echo -e "\t-u\tInstall/update vim plugins"
    echo -e "\t-l\tLink dotfiles into your home directory"
    echo -e "\t-a\tSame as -ul"
}


while getopts 'hula' OPT
do
    case $OPT in
        h)
            usage
            ;;
        u)
            update_vim_plugins
            ;;
        l)
            link_dotfiles
            ;;
        a)
            link_dotfiles
            echo ""
            update_vim_plugins
            ;;
    esac
done
